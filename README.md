## 1. HTTP protocol

Là cách thức website được truyền tải qua internet.

Request được gửi đi, server nhận được request, lấy dữ liệu từ trong kho & gửi về Response. Browser đọc source code server gửi về & hiển thị ra.

**Note**: Xem chi tiết của request tại tab Network/All của cửa sổ inspect.

- HTTP Request Methods: GET, POST, ...
- HTTP Response (phản hồi về của server)
- Status Code (Mã phản hồi): 
    - 1xx - Thông tin 
    - 2xx - Thành công 
    - 3xx - Điều hướng lại 
    - 4xx - Lỗi client
    - 5xx - Lỗi server.


## 2. SSR & CSR

Mã HTML & CSS được render ra ở đâu? ('View Page Source' để thấy khác biệt).

**SSR (Server side rendering)**

- Mã HTML & CSS được render ra ở Server.
- Thường được sd trong website cần yếu tố SEO, tăng thứ hạng tìm kiếm trên google.
- Toàn bộ code HTML giao diện do server trả thẳng về. Browser đọc mã & render ra giao diện.
- Ưu điểm: Tốt cho SEO; Lượt truy cập đầu tiên sẽ nhanh hơn.
- Khuyết điểm: Mỗi lần chuyển hướng trang sẽ phải reload lại toàn bộ website.

**CSR (Client side rendering)**

- Mã HTML & CSS được render ra ở Client.
- Lần đều tiên load website, server trả về đoạn mã HTML với 1 thẻ div trống để chứa content của website. Client browser đọc file JS được trả về kèm, xử lý & append mã HTML content rồi render ra giao diện.
- Ưu điểm: Khi chuyển hướng không cần reload lại toàn bộ trang, chỉ cần load thêm phần cần thiết -> website mượt mà hơn, các lượt truy cập sau sẽ nhanh hơn. 
- Khuyết điểm: Không tốt cho SEO; Lần đầu tiên truy cập sẽ lâu hơn.

**Note**: Trong ứng dụng này làm nodejs với SSR, -> làm API phục vụ cho xây dựng ứng dụng CSR.


## 3. NodeJS

- Browser cần JS engine (lõi) để có thể chạy được JS.
- NodeJS là 1 nền tảng JS (JS runtime) được xây dựng dựa trên JS engine (trình thông dịch JS).


## 4. Express

- Expressjs là một framework được xây dựng trên nền tảng của Nodejs.  
- Expressjs hỗ trợ các HTTP method và middleware tạo ra API mạnh mẽ.


## 5. Initialize project

entry point (index.js) là file chạy đầu tiên khi ứng dụng chạy.  
**package.json** chứa toàn bộ thông tin cấu hình của ứng dụng.
**package-lock.json** hay **yarn.lock** quản lý dependencies chi tiết hơn.

- *dependencies* chứa thư viện mà ứng dụng cần khi chạy.
- *devDependencies* chứa thư viện chỉ sd trong quá trình dev phát triển ứng dụng, khi website chạy thật thì không cần.
- *scripts* chứa object cấu hình các dòng lệnh thực thi: 
    ```json
    "scripts": {} 
    ``` 


## 6. Debug (inspector)

- Trong cửa sổ inspect, ấn mở dev tool của NodeJS.
- Đặt Breakpoint ngăn chặn sự hoàn thiện thực thi request. Ta có thể điều khiển quá trình thực thi.  


## 7. [Nodemon](https://www.npmjs.com/package/nodemon) library

Phát hiện sự thay đổi trong source code & tự động restart ứng dụng.  

Command line:  
```js
"scripts": {
    "start": "nodemon --inspect src/index.js", // flag --inspect giúp ích cho debug
    "ignore": "nodemon --inspect --ignore 'lib/*.js' src/index.js"
} 
```

Có thể cấu hình nodemon với *nodemon.json* hoặc tại *package.json* hay các flag options của Command line (--ignore,...):      
```js
{
    "ext": "js mjs json", // Theo dõi các file có phần mở rộng .js, .mjs, .json
    "ignore": ["test/*", "docs/*"], // Bỏ theo dõi các file trong thư mục test & docs
}
```


## 8. Module

Là đoạn mã được đóng gói thành file riêng biệt & mang tính Private. Có thể tái sử dụng trong ứng dụng.  

Định nghĩa module:  

- **module** là object đại diện cho module hiện tại.  
- **module.exports** là obj xác định value mà module exports.
    ```js
    const log = {
        info: function (info) { console.log('Info: ' + info); },
        warn: function (warn) { console.warn('Warning: ' + warning); },
        error: function (error) { console.error();('Error: ' + error); }
    };
    module.exports = log
    ```  
- **module.exports** là một object nên ta có thể export bất cứ giá trị nào (eg: fn, array, obj, class, string, number, boolean) thông qua property:  
    ```js
    function favouriteBook() {
        return { title: "The Guards", author: "Ken Bruen" };
    }
    module.exports.favoriteBook = favoriteBook;
    ```  
    **Note**: shorthand khi export thông qua property là **exports.nameProperty = value**  
    ```js
    let film101 = { name: 'Mr Caruthers', country: 'USA' }
    exports.film101 = film101
    ```

Import và truy xuất module: **require(path)**  
```js
const books = require('Books')
const favouriteBook = books.favouriteBook()
console.log(`My favourite book is ${favouriteBook}'s ${favouriteBook.title}`);
```


## 9. MVC model

Là design pattern áp dụng khi xây dựng ứng dụng.  

Ưu điểm: Bóc tách ra 3 thành phần chính: model - view - controller.  

- model: tương tác với data source.   
- view: chứa UI giúp user tương tác với hệ thống.  
- controller: trung chuyển giữa view & model.  

**Flow**: Browser truy cập website, sẽ request lên web server với HTTP protocol, server chọc tới tầng Router, dispatch controller tương ứng. Controller sẽ tương tác với model lấy data trong database nếu cần, sau đó gọi view để lấy view tương ứng, tại controller truyền data sang view, khi đó nhận được view hoàn chỉnh, trả về cho web server, web server phản hồi lại client qua response HTTP protocol, browser nhận được & hiển thị ra website.  


## 10. Format code  

**prettier** library: giúp format lại code theo quy chuẩn thống nhất trước khi commit lên git.  
```json
"scripts": {
    "beautiful": "prettier --single-quote --trailing-comma all --tab-width 4 --write src/**/*.{js,json,scss}"
}
```

**lint-staged** library: giúp chạy một command trên những file matched với pattern đã được add vào git (staged files).  
```json
// Tự động thực thi command prettier với những staged file
"lint-staged": {
    "src/**/*.{js,json,scss}": "prettier --single-quote --trailing-comma all --tab-width 4 --write"
},
"scripts": {
    "beautiful": "lint-staged"
}
```  

**husky** library: cung cấp hooks, giúp tự động làm gì đó khi thực hiện hành động với git như: git commit, git push, ....  
```json
// Tự động chạy lint-staged khi trước khi commit
"husky": {
    "hooks": {
      "pre-commit": "lint-staged "
    }
  },
```


### Note: <script\> async & defer attribute

JS dù là inline hay external đều có thể làm chậm quá trình load webpage, để kiểm soát quá trình fetch & thực thi JS external, *script* có 2 attribute: async & defer.  

**<script\>**:  parse HTML file đến khi gặp script tag, lúc này quá trình parse tạm dừng, nêu là external script thì sẽ tạo một request để fetch script, thực thi script hoàn tất thì quá trình parse sẽ tiếp tục.  

**<script async src=""\>**: parse HTML file đến khi gặp script tag, download script song song với quá trình parse, tạm dừng parse & thực thi script ngay khi download hoàn tất, thực thi xong thì sẽ tiếp tục parse.  

**<script defer src=""\>**: parse HTML file đến khi gặp script tag, download script song song với quá trình parse, thực thi script khi parse hoàn tất.  

**Note**:  

- *async*: Không đảm bảo thứ tự thực thi các script được nhúng vào page, do script được thực thi ngay sau khi load xong.  
- *defer*: Đảm bảo thứ tự thực thi các script được nhúng vào page.    

**Using**:  

- script độc lập hay có sự phụ thuộc hay có tương tác với DOM?.  
- <script\> tag nằm ở đâu trong page? 