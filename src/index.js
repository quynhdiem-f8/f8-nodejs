const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');
const port = 5000;

const route = require('./routes');

// Start server
const app = express();

// Static file
app.use(express.static(path.join(__dirname, 'public')));

// middleware handles data from client
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ extended: true }));

// HTTP logger
// app.use(morgan('combined'))

// Template engine
app.engine('hbs', exphbs({ extname: '.hbs' }));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources/views'));

// Routes
route(app);

// Start server lắng nghe các connection tại port 5000
app.listen(port, () =>
    console.log(`Example app listening at http://localhost:${port}`),
);
