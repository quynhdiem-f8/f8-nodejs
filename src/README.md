## 1. Start web server

Xem chi tiết tại [Express](http://expressjs.com/en/api.htm).  

- Khởi tạo *Express server*:
    ```js
    const express = require('express') // nạp thư viện express đã cài trong node_modules
    const app = express() // trả về instance obj đại diện cho ứng dụng
    ```

- Định nghĩa route:
    ```js
    app.get('/', (req,res) => {
        res.send('Hello World') // server gửi response với content là string "Hello World" đến client
    }) 
    ```
    **Note**: [res.send(body)](http://expressjs.com/en/api.htm#res.send) -  gửi HTTP response đến client.

- Start web server lắng nghe các kết nối trên port 5000:
    ```js
    const port = 5000
    app.listen(port,()=>console.log(`Example app listening at http://localhost:${ port }`)) 
    ```  
    **Note**: [app.listen()](http://expressjs.com/en/4x/api.html#app.listen) - trả về http.Server object.  


**Note**: Để set **https** protocol server:  
```js
const https = require('https')
https.createServer(app).listen(port, ()=>console.log(`Example app listening at http://localhost:${ port }`))
```

Express framework giúp cấu hình web server & route.  

**Flow**: Khi chạy command line start web server, nodejs sẽ chọc vào file entry point nạp content vào RAM. Khi truy cập website, request được gửi đến web server, server đọc dữ liệu route trong RAM, checks request URL path matched với route path, execute handler func (action -> dispatcher -> handler).


## 2. HTTP logger

Sử dụng [Morgan](https://www.npmjs.com/package/morgan) library: Middleware func để logger các HTTP request từ phía client lên node server.

```js
const morgan = require('morgan')
app.use(morgan('combined')) // combined là kiểu logger
```

**Note**: [app.use(path, callback)](http://expressjs.com/en/api.htm#app.use) - define các middleware layer cho ứng dụng với path xác định, nếu không truyền path thì mặc định là '/'.


## 3. Template engines

Là công cụ giúp chia layout HTML, đạt được hiệu quả tương tự như viết HTML thủ công.  

Một số template engine phổ biến: express-handlebars, pug, ...  

Ứng dụng này sử dụng [express-handlebars](https://www.npmjs.com/package/express-handlebars) template engine.  


*express-handlebars* có một số [built-in helpers](https://handlebarsjs.com/guide/builtin-helpers.html): if, each, with, log, ...

```js
const exphbs = require('express-handlebars')
const path = require('path')

app.engine('hbs', exphbs({extname: '.hbs'})); // app sd template engine express-handlebars với tên 'hbs', có phần mở rộng tệp '.hbs'
app.set('view engine', 'hbs'); // set 'view engine' setting của app là hbs
app.set('views', path.join(__dirname, 'resources/views')) // set 'views' directory setting của app là 'src/resources/views'

// Route
app.get('/', (req,res) => {
        res.render('home') // hiển thị view 'home'
    }) 
```

Hoặc có thể sử dụng *express-handlebars* với việc tạo instance:  

```js
const exphbs = require('express-handlebars')
const path = require('path')

const hbs = exphbs.create({extname: '.hbs'}) // khởi tạo instance
app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources/views')) 
```

**Note**:  

- **exphbs.create({ config })**: Khởi tạo instance của express-handlebars.  
- [app.engine(name, callback)](http://expressjs.com/en/api.htm#app.engine) - App sử dụng template engine 'callback' với tên 'name'.  
- [app.set(name, value)](http://expressjs.com/en/api.htm#app.set) - Gán giá trị cho property 'Application setting'.  
- [app.get(name)](http://expressjs.com/en/api.htm#app.get) - Trả về giá trị property 'Application setting'.  
- [res.render(view, data)](http://expressjs.com/en/api.htm#res.render) - Render view & gửi chuỗi HTML kết xuất tới client, data - dữ liệu dạng obj muốn hiển thị trên view (optional).  
- [path](https://nodejs.org/api/path.html) module:  
    - **path.join(\[...paths\])** trả về path là kết quả khi nối tất cả các chuỗi ngăn cách bởi '/'.  
    - **path.basename(path)** trả về phần cuối cùng của path (tệp).  
    - **path.dirname(path)** trả về tên thư mục của path.  
    - **path.extname(path)** trả về phần mở rộng của path.  
    - **\_\_dirname** trả về path của thư mục chứa file đang chạy.  


## 4. Static file

Static file: image, css, js, ... (localhost:5000/img/logo.png)  

Cấu hình để sử dụng file tĩnh:  
```js
const path = require('path')
app.use(express.static(path.join(__dirname, 'public'))) // kiểm tra url path là static file thì sẽ vào thư mục public
```

**Note**: [express.static(root)](http://expressjs.com/en/api.html#express.static) là built-in middleware func cung cấp static file, root là thư mục gốc quản lý các file tĩnh.


## 5. [SCSS](https://sass-lang.com/guide)

Webpage không thể đọc được các file scss, chỉ có thể đọc được các file css được compile từ file scss.

Chỉ viết code ở các file scss, tuyệt đối không viết tại file css, file css là kêt quả build ra từ file scss.

Các thư viện giúp compile file scss thành file css như: [node-sass](https://www.npmjs.com/package/node-sass), [sass](https://www.npmjs.com/package/sass), ...  

Command line để compile file scss sang file css:  

- node-sass:
    ```json
    "scripts": {
        "scss": "node-sass --watch src/resources/scss/ --output src/public/css/"
    },
    ```

- sass:
    ```json
    "scripts": {
        "scss": "sass --watch --no-source-map src/resources/scss/:src/public/css/"
    },
    ```

**Note**: option --watch (-w) để theo dõi sự thay đổi của các file scss input & tự động compile lại.


## 6. [Bootstrap](https://getbootstrap.com/docs/4.6/components/alerts/)

Là framework phổ biến cho việc xây dựng web ***responsive***.


## 7. [Request](http://expressjs.com/en/4x/api.html#req) & [Response](http://expressjs.com/en/4x/api.html#res)

Là Stream Object.

**request** - chứa các thông tin mà client gửi đến server.  
> **req.baseUrl**   - Trả về path mà router obj được mount.  
> **req.cookies**   - Obj chứa cookie được gửi bởi request (cookie-parser middleware).  
> **req.params**    - Obj chứa route parameters.  
> **req.query**     - Obj chứa query string parameters.  
> **req.body**      - Chứa các cặp key-value được gửi vào request body. (body-parsing middleware).  
> **req.signedCookies** - Obj chứa signed cookie.  
> **req.get()**     - Trả về HTTP request header field.  

**response** - giúp tùy chỉnh các thông tin trả về cho client.  
> **res.send()**        - Gửi một response.  
> **res.json()**        - Gửi một JSON response.  
> **res.sendStatus()**  - set response HTTP status code & gửi status message.  
> **res.render()**  	- Render a view template.  
> **res.redirect()**    - Redirect a request.  
> **res.status()**      - Set HTTP status code cho response.  
> **res.set()**         - Set HTTP Header cho response.  
> **res.get(field)**    - Trả về HTTP response Header field.  
> **res.cookie()**      - Set cookie.  
> **res.clearCookie()** - Xóa cookie.  
> **res.end()**         - Kết thúc một response process.  


## 8. Routing

Định nghĩa ra các tuyến đường để tạo ra điểm truy cập (endpoints - URI) cho website.

Mỗi route có một hoặc nhiều handler func mà được thực thi khi uri khớp với route.

Khi routing method có nhiều handler thì ngoại trừ handler cuối, các handler còn lại cần có next làm callback & gọi trong thân hàm để chuyển quyền kiểm soát cho handler tiếp theo. 

Syntax: **app.METHOD(path, handler)**    
path: Có thể là string, string pattern hay regular expression.  
METHOD: HTTP request method (get, post, put, patch, ...)  

```js
app.get('/', (req, res) => res.render('home'))

app.get('/search', (req, res, next) => {
    console.log('the response will be sent by the next function ...');
    next()
}, (req, res) => res.render('search'))
```  


## 9. express.Router()  

Tạo một router object mới như một "mini-app" có hệ thống middleware & router hoàn chỉnh, có thể module hóa.  

Vì tương tự "mini-app" nên có thể thêm middleware & HTTP method route cho router object. (note: router chính là *group route*)  

router object hành xử như một middleware, nên có thể làm đối số cho **app.use()** hoặc **router.use()** để mount router vào main-app hay router khác.   

- Tạo file birds.js:  
    ```js
    const express = require('express')
    const router = express.Router()

    // middleware that is specific to this router
    router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now())
    next()
    })

    // define routes
    router.get('/', (req, res) => res.send('Birds home page'))
    router.get('/about', (req, res) => res.send('About birds')})

    module.exports = router
    ```  

- Gắn module router birds vào một path của main-app:  
    ```js
    const birds = require('./resources/modules/birds')
    app.use('/birds', birds)
    ```  


## 10. HTTP request method

Là method của HTTP protocol.  

- GET: Sử dụng để lấy dữ liệu từ server về client.  
- POST: Sử dụng để gửi dữ liệu từ client lên server.  
- PUT: Sử dụng để thay đổi toàn bộ dữ liệu.  
- PATCH: Sử dụng để sửa đổi một phần của dữ liệu.  
- DELETE: Sử dụng để xóa dữ liệu.  


## 11. Query parameters 

Sử dụng để truyền dữ liệu qua URL.  

Syntax: *?key1=value1&key2=value2*  

Giá trị của 'query parameters' được lưu tại obj **req.query**.

```js
// URL: http://localhost:5000/search?q=f8%20nodejs&author=son%20dang
app.get('/search', (req,res) => {
    console.log(req.query); // {q: 'f8 nodejs', author: 'son dang'}
    return res.render('search')
})
```


## 12. Route parameters

Sử dụng để lấy giá trị của URL segment.  

Syntax: */:key*  
> /user/:userId(\d+)  
> /flights/:from-:to  
> /plantae/:genus.:species  

Giá trị của 'route parameter' được lưu tại obj **req.params**.  

```js
// URL: http://localhost:5000/users/34/books/8989
app.get('/users/:userId/books/:bookId(\d+)', (req, res) => {
    console.log(req.params); // { "userId": "34", "bookId": "8989" }
    return res.send(req.params)
})
```


## 13. Form default behavior

Khi submit form sẽ tạo request mới tới server với method được xác định tại *method* attribute của form & path được xác định tại *action* attribute của form. Request chứa dữ liệu được nhập của form được gửi tới server.

Method mặc định là GET method & path mặc định là path hiện tại. *Request với **GET method** gửi dữ liệu đến server dưới dạng query parameter*, nên mặc định khi submit form dữ liệu từ input có attribute name của form sẽ tự động đính lên URL dưới dạng query parameter & load lại trang.   

*Request với **POST method** gửi dữ liệu đến server dưới dạng khác như 'urlencoded' (eg.'Form data') hay 'JSON'*, nên khi submit form với method post sẽ không đính thêm query parameters lên URL.  

Server nhận được request, dựa vào request method & path mà định tuyến đến route có path phù hợp. Trước khi thực thi handler func của route, request cần đi qua middleware.  

Server nhận được request nhưng dữ liệu chưa được lưu lại, cần có middleware để parse request body, xử lý dữ liệu & lưu lại. Express đã tích hợp sẵn middleware parse 'query parameter' & lưu tại **req.query**, ứng dụng cần apply các middleware parse 'other bodies' khác.    

Một số built-in middleware func của Express dựa trên body-parser middeware giúp parse request body & lưu tại **req.body**:  
    - **express.urlencoded()** - parse 'urlencoded'.  
    - **express.json()** - parse 'json'.  
    - **express.text()** - parse 'all bodies' -> string.  
    - **express.raw()** - parse 'all bodies' -> Buffer obj.

```js
// middleware parse request body to handle data from client
app.use(express.urlencoded({ extended: true }))
app.use(express.json({ extended: true }))

// gets request body
app.get('/search', (req,res) => res.render('search', req.query))
app.post('/search', (req,res) => res.render('search', req.body))
```

**Note**:  

- Khi submit với method POST thì dù reload lại vẫn sẽ là POST method.  
- Khi nhập URL vào thanh địa chỉ & enter thì mặc định request được gửi đi với method GET.    