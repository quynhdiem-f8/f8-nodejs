const express = require('express');
const router = express.Router();

const newsController = require('../app/controllers/NewsController');

router.post('/', (req, res) => {
    return res.render('news', req.body);
});
router.use('/:slug', newsController.show);
router.use('/', newsController.index);

module.exports = router;
